import { API_VIDEO_ENTITY } from './Type';
import { ApiType, API_VIDEO_UPLOAD_PROCESS } from '../baseType';

export const authApiVideo2 = () => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: API_VIDEO_ENTITY,
      actionStatusName: 'postVideo',
      method: 'get',
      url: '/api/apiVideo/auth',
    },
    data: {},
  },
});

export const authApiVideo = () => ({
  type: ApiType.Video,
  payload: {
    meta: {
      entity: API_VIDEO_ENTITY,
      actionStatusName: 'postVideo',
      method: 'POST',
      url: '/auth/api-key',
      endpoint: '/auth/api-key',
      body: {
        'apiKey': '2k1JBW5AQpWYM4zSAqoTIb7izuKQb5ijqBmnWeigF5p',
      },
    },
  },
});

export const createVideoObject = (title, desc) => ({
  type: ApiType.Video,
  payload: {
    meta: {
      entity: API_VIDEO_ENTITY,
      actionStatusName: 'postVideo',
      method: 'POST',
      url: '/videos',
      endpoint: '/videos',
      baseUrl: 'https://ws.api.video',
      body: {
        'title': title,
        'description': desc,
      },
    },
  },
});

export const uploadVideo = (formdata, endpointtmp) => ({
  type: ApiType.Video,
  payload: {
    meta: {
      entity: API_VIDEO_ENTITY,
      actionStatusName: 'postVideo',
      method: 'POST',
      url: endpointtmp,
      endpoint: '/videos',
      baseUrl: 'https://ws.api.video',
      body: formdata,
      header: {
        'Content-Type': 'multipart/form-data',
      },
    },
  },
});

export const uploadVideoFlow = (formdata, title, desc) => ({
  type: API_VIDEO_UPLOAD_PROCESS,
  payload: {
    meta: {
      entity: API_VIDEO_ENTITY,
      isTranslation: false,
      actionStatusName: 'postVideo',
      formdata,
      title,
      desc,
    },
    data: {},
  },
});

export const uploadVideoTranslationFlow = (formdata, title, desc) => ({
  type: API_VIDEO_UPLOAD_PROCESS,
  payload: {
    meta: {
      entity: API_VIDEO_ENTITY,
      isTranslation: true,
      actionStatusName: 'postVideo',
      formdata,
      title,
      desc,
    },
    data: {},
  },
});
