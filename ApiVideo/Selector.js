import store from '../../flux/redux';
import baseSelector from '../baseSelector';

export default class ApiVideoHelper extends baseSelector {
  static getHeaderAuthorization() {
    const state = store.getState()['apivideo'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('videosApi')) {
      return null;
    }
    if (!state.get('videosApi').get('videoSession')) { return null; }
    return `Bearer ${state.get('videosApi').get('videoSession').get('accessToken')}`;
  }
  static getApiVideoStatus() {
    const state = store.getState()['apivideo'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('status')) {
      return null;
    }
    return state.get('status');
  }

  static getApiVideoError() {
    const state = store.getState()['apivideo'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('error')) {
      return null;
    }
    return state.get('error').toJS();
  }

  static getApiVideoSession() {
    const state = store.getState()['apivideo'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('videos')) {
      return null;
    }
    return state.get('videos').toJS();
  }

  static getApiVideoToken() {
    const state = store.getState()['apivideo'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('videosApi')) {
      return null;
    }
    if (!state.get('videosApi').get('videoSession')) { return null; }
    return state.get('videosApi').get('videoSession').get('accessToken');
  }

  static getPostVideoStatus() {
    return super.getStatusByName('pullDocument');
  }
}
