import { fromJS } from 'immutable';
import { Status } from '../baseType';
import { API_VIDEO_ENTITY, API_REMOVE_VIDEOS } from './Type';
import { baseReducer } from '../baseReducer';

const initialState = fromJS({
  status: Status.Default,
  error: {},
  videos: {},
});

export const ApiVideoReducer = (state = initialState, action) => {
  switch (action.type) {
    case API_REMOVE_VIDEOS:
      return initialState;
    default:
      return baseReducer(state, action, API_VIDEO_ENTITY, 'videos');
  }
};
