import BaseApi from './BaseApi';
import ApiVideoHelper from '../../../flux/ApiVideo/Selector';

export default class ApiVideo extends BaseApi {
  constructor(token) {
    super();
    this.baseUrl = 'https://ws.api.video';
    this.endpoint = '';
    this.token = token;
  }

  _header(headers) {
    if (headers == null) {
      headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      };
    }
    const headerAuthorization = this.token ? this.token : ApiVideoHelper.getHeaderAuthorization();
    if (headerAuthorization) {
      headers['Authorization'] = headerAuthorization;
    }
    return headers;
  }

  setEndpoint(endpoint) {
    this.endpoint = endpoint;
    return this;
  }

  forbidenAccess(response) {
  }

  formatResponse(response) {
    let videoresp = {
      'data': [
        {
          ...response.data,
          'type': this.endPointToType(this.endpoint),
          'id': response.data['videoId'] ? response.data['videoId'] : 'videoSession',
        },
      ],
      'includes': [],

    };
    let a = {
      status: response.status,
      data: videoresp,
    };
    return a;
  }

  endPointToType(endpoint) {
    switch (endpoint) {
      case '/videos':
        return 'videos';
      case '/auth/api-key':
        return 'videos';
      default:
        return 'default';
    }
  }
}
