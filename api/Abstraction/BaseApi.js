import { fromJS } from 'immutable';
import axios from 'axios';
import SessionHelper from '../../Session/Selector';

export default class BaseApi {
  constructor() {
    // if (new.target === BaseApi) {
    // throw new TypeError('Cannot construct Abstract instances directly');
    // }
    this._header = this._header.bind(this);
    this._formatArray = this._formatArray.bind(this);
    this._serializeResp = this._serializeResp.bind(this);
    this.formatResponse = this.formatResponse.bind(this);
    this.call = this.call.bind(this);
  }

  _header() {
    throw new Error('You have to implement the method _header()');
  }

  call({ method, url, body, fake, header }) {
    let headers = this._header(header);
    if (header === null) {
      throw new Error('Method "header" have to return instance class.');
    }
    if (method === null || url === null) {
      throw new Error('Param of "call" need method, url');
    }
    if (method === 'post' && body === null) {
      throw new Error('Body of post request cn\'t be null');
    }
    return axios({
      method: method,
      url: url,
      baseURL: fake === true ? 'http://localhost:4242/' : this.baseUrl,
      data: body,
      headers: headers,
    }).catch(error => {
      if (error && error.response && error.response.data) {
        if (error.response.status === 401 && SessionHelper.hasSessionActive()) {
          this.forbidenAccess(error.response);
        }
        return Promise.reject({ data: error.response.data, status: error.response.status });
      } else {
        return Promise.reject({ data: 'Network Error', status: 'failed' });
      }
    }).then((response) => {
      let formatedResonse = this.formatResponse(response);
      if (formatedResonse === null) { formatedResonse = {}; }
      return Promise.resolve(this._checkFormat(formatedResonse));
    }).then(this._json).then(this._serializeResp);
  }

  formatResponse(response) {
    // Format la réponse pour qu'elle ressemble à celle de l'api dialogram
    throw new Error('You have to implement the method _formatResponse(response)');
  }
  forbidenAccess(response) {
    throw new Error('You have to implement the method forbidenAccess(response)');
  }
  _json(response) {
    if (response.status !== 204) {
      return response.data || response.json();
    } else {
      return {};
    }
  }

  _mapType(type) {
    if (type) { type = type.toLowerCase(); }
    switch (type) {
      case 'webfile':
      case 'weblink':
      case 'googlefile':
      case 'evernotefile':
        return 'media';
      default:
        return type;
    }
  }

  _formatArray(included) {
    const keyedIncluded = {};
    included.forEach((data) => {
      const type = this._mapType(data.type);
      let id;
      if (data.type === 'pagination') { id = data.id ? data.id : 'pagination'; } else { id = data.id ? data.id : 'videoSession'; }
      if (type) {
        if (!keyedIncluded[type]) {
          keyedIncluded[type] = {};
        }
        keyedIncluded[type][id] = data;
      } else {
        keyedIncluded[id] = data;
      }
    });
    return keyedIncluded;
  }

  _serializeResp(json) {
    return new Promise((resolve) => {
      json.data = this._formatArray(json.data);
      if (json.includes) {
        json.includes = this._formatArray(json.includes);
      } else {
        json.includes = [];
      }
      const immutableData = fromJS(json);
      resolve(immutableData);
    });
  }
  /*
  _status(response) {
    if (response && response.status) {
      if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response);
      } else if (response.status === 401 && SessionHelper.hasSessionActive()) {
        console.log("IF =>", response)
        this.forbidenAccess(response);
      }
      console.log("DEFAULT =>", response)
      return Promise.reject(response);
    } */

  _checkFormat(response) {
    if (!response['data']) {
      console.error('response have no child called "data"');
      return response;
    }
    if (!response['data']['data']) {
      console.error("response['data'] have no child called \"data\"");
      return response;
    }
    return response;
  }
}
