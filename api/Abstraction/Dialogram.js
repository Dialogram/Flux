import BaseApi from './BaseApi';
import SessionHelper from '../../../flux/Session/Selector';

export default class DialogramApi extends BaseApi {
  constructor() {
    super();
    this.baseUrl = window.APP_CONFIG.API_ROOT;
  }

  _header(headers) {
    if (headers == null) {
      headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      };
    }
    const headerAuthorization = SessionHelper.getHeaderAuthorization();
    if (headerAuthorization) {
      headers['Authorization'] = headerAuthorization;
    }
    return headers;
  }

  forbidenAccess(response) {
  //  SessionHelper.deleteSessionData();
  }

  formatResponse(response) {
    return response;
  }
}
