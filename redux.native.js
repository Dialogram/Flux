import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import immutableTransform from 'redux-persist-transform-immutable';
import createSagaMiddleware from 'redux-saga';

import { videoReducer } from './Videos/Reducer';
import { sessionReducer } from './Session/Reducer';
import { userReducer } from './User/Reducer';
import { watcherSaga } from './sagas/index';
import { documentReducer } from './Documents/Reducer';
import { translationReducer } from './Translations/Reducer';
import { ApiVideoReducer } from './ApiVideo/Reducer';
import { searchReducer } from './Search/Reducer';

import { TranslationVideoReducer } from './TranslationVideo/Reducer';

import {
  statusReducer,
} from './statusReducer';

const sagaMiddleware = createSagaMiddleware();
const reducer = combineReducers({
  session: sessionReducer,
  user: userReducer,
  videos: videoReducer,
  documents: documentReducer,
  translations: translationReducer,
  apivideo: ApiVideoReducer,
  search: searchReducer,
  ui: statusReducer,
  TranslationVideo: TranslationVideoReducer
});

const persistConfig = {
  key: 'root',
  storage,
  transforms: [immutableTransform()],
  whitelist: [
    'session',
    'user',
  ],
  timeout: 0,
};

const persistedReducer = persistReducer(persistConfig, reducer);

const middlewares = [sagaMiddleware];

if (process.env.NODE_ENV === 'development' && __DEV__) {
  const { logger } = require('redux-logger');
  middlewares.push(logger);
}

const store = createStore(persistedReducer, compose(applyMiddleware(...middlewares)));

sagaMiddleware.run(watcherSaga);

export const loadPersistStore = (cb) => {
  return persistStore(store, null, cb);
};

export const dispatch = store.dispatch;

export default store;
