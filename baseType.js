export const Status = {
  Default: 'Default',
  Loading: 'Loading',
  Success: 'Success',
  Failure: 'Failure',
};

export const ApiType = {
  Dialogram: 'DialogramApi',
  Video: 'VideoApi',
  RNBlob: 'RNBlob',
};

export const SET_ENTITY = '[SET_ENTITY]';
export const LOG_OUT = '[LOG_OUT]';
export const API_VIDEO_UPLOAD_PROCESS = '[API_VIDEO_UPLOAD_PROCESS]';
