import store from '../../flux/redux';

export default class SearchHelper {
  static getUserList() {
    const state = store.getState()['search'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('user')) {
      return null;
    }
    return state.get('user').toJS();
  }
}
