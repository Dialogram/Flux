import { fromJS } from 'immutable';
import { Status } from '../baseType';
import { REMOVE_USERS_LIST, SEARCH_ENTITY } from './Type';
import { baseReducer } from '../baseReducer';

const initialState = fromJS({
  status: Status.Default,
  error: {},
  user: {},
});

export const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case REMOVE_USERS_LIST:
      return state;
    default:
      return baseReducer(state, action, SEARCH_ENTITY, 'videos');
  }
};
