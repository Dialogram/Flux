import { ApiType } from '../baseType';
import { REMOVE_USERS_LIST, SEARCH_ENTITY, SEARCH_USER_FLOW } from './Type';

export const removeUserList = () => ({
  type: REMOVE_USERS_LIST,
  payload: {
    meta: {},
    data: {},
  },
});

export const searchUserFlow = ({ toFind }) => ({
  type: SEARCH_USER_FLOW,
  payload: {
    meta: {
      toFind,
    },
    data: {},
  },
});

export const searchUser = ({ toFind }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: SEARCH_ENTITY,
      actionStatusName: 'searchUser',
      method: 'get',
      url: `/api/search/user/${toFind}`,
    },
    data: {},
  },
});
