import { NavigationActions } from 'react-navigation';

class NavigationFlux {
  constructor() {
    this._navigator = null;
  }

  setTopLevelNavigator(navigatorRef) {
    this._navigator = navigatorRef;
  }

  navigate(routeName, params) {
    this._navigator && this._navigator.dispatch(
      NavigationActions.navigate({
        routeName: routeName,
        params,
      })
    );
  }

  goBack() {
    this._navigator && this._navigator.dispatch(
        NavigationActions.back()
    );
  }
}

export default new NavigationFlux();
