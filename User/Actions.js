import { ApiType } from '../baseType';
import { USER_ENTITY, REMOVE_USER, DELETE_ACCOUNT_FLOW, RESET_USER_ERROR } from './Type';

export const resetUserError = () => ({
  type: RESET_USER_ERROR
})

export const confirmMail = ({ token }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: USER_ENTITY,
      actionStatusName: 'register',
      method: 'get',
      url: `/api/user/settings/account/email/${token}`,
    },
  },
});
export const resendConfirmToken = () => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: USER_ENTITY,
      actionStatusName: 'register',
      method: 'put',
      url: `/api/user/send/verification`,
    },
  },
});

export const confirmAccount = ({ token }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: USER_ENTITY,
      actionStatusName: 'register',
      method: 'get',
      url: `/api/user/confirm/${token}`,
    },
  },
});

export const deleteAccountFlow = () => ({
  type: DELETE_ACCOUNT_FLOW,
  payload: {
    meta: {},
    data: {},
  },
});

export const deleteAccount = () => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: USER_ENTITY,
      actionStatusName: 'deleteAccount',
      method: 'DELETE',
      url: `/api/user/settings/account/delete`,
    },
  },
});

export const updateProfilePicture = (formData) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: USER_ENTITY,
      actionStatusName: 'updateAccount',
      method: 'post',
      url: '/api/user/settings/profilePicture',
      body: formData,
    },
  },
});

export const getUser = () => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: USER_ENTITY,
      actionStatusName: 'login',
      method: 'get',
      url: '/api/user/',
    },
  },
});

export const createUser = ({ nickName, firstName, lastName, email, password, from }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: USER_ENTITY,
      actionStatusName: 'register',
      method: 'post',
      url: '/api/user',
      body: {
        nickName,
        profile: {
          firstName,
          lastName,
        },
        email,
        password,
      },
      navigateTo: from,
    },
  },
});

export const updateUserPublic = ({ firstName, lastName, birthday, gender, country, hometown, description }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: USER_ENTITY,
      actionStatusName: 'updateAccount',
      method: 'PUT',
      url: '/api/user/settings/public',
      body: {
        profile: {
          lastName,
          firstName,
          birthday,
          gender,
          country,
          hometown,
          description
        },
      },
    },
  },
});

export const updateUserPrivate = ({ nickName, email }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: USER_ENTITY,
      actionStatusName: 'updateAccount',
      method: 'PUT',
      url: '/api/user/settings/account',
      body: {
        nickName,
        email,
      },
    },
  },
});

export const updatePassword = ({ currentPassword, newPassword }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: USER_ENTITY,
      actionStatusName: 'updatePassword',
      method: 'put',
      url: '/api/user/settings/password',
      body: { currentPassword, newPassword },
    },
  },
});

export const removeUser = () => ({
  type: REMOVE_USER,
});
