import store from '../../flux/redux';
import baseSelector from '../baseSelector';

export default class UserHelper extends baseSelector {
  static getUserId() {
    const state = store.getState()['user'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('user')) {
      return null;
    }
    return state.get('user').first().get('id');
  }

  static getUserStatus() {
    const state = store.getState()['user'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('status')) {
      return null;
    }
    return state.get('status');
  }

  static getUserError() {
    const state = store.getState()['user'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('error')) {
      return null;
    }
    return state.get('error');
  }

  static getUserNickname() {
    const state = store.getState()['user'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('user')) {
      return null;
    }
    return state.get('user').first().get('nickName');
  }

  static getUserProfile() {
    const state = store.getState()['user'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('user')) {
      return null;
    }
    return state.get('user').first().get('profile').toJS();
  }

  static getUserEmail() {
    const state = store.getState()['user'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('user')) {
      return null;
    }
    return state.get('user').first().get('email');
  }

  static getUserFirstName() {
    const state = store.getState()['user'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('user')) {
      return null;
    }
    return state.get('user').first().get('profile').get('firstName');
  }

  static getUserLastName() {
    const state = store.getState()['user'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('user')) {
      return null;
    }
    return state.get('user').first().get('profile').get('lastName');
  }

  static getUserprofilePictureUrl() {
    const state = store.getState()['user'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('user')) {
      return null;
    }
    return state.get('user').first().get('profile').get('profilePicture').get('url');
  }

  static getRegisterStatus() {
    return super.getStatusByName('register');
  }

  static getDeleteAccountStatus() {
    return super.getStatusByName('deleteAccount');
  }

  static getUpdatePasswordStatus() {
    return super.getStatusByName('updatePassword');
  }

  static getUpdateAccountStatus() {
    return super.getStatusByName('updateAccount');
  }
}
