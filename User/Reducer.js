import { fromJS } from 'immutable';
import { Status, SET_ENTITY } from '../baseType';
import { USER_ENTITY, REMOVE_USER, RESET_USER_ERROR } from './Type';
import { baseReducer } from '../baseReducer';

export const initialState = fromJS({
  status: Status.Default,
  error: {},
});

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case `${USER_ENTITY} ${SET_ENTITY}`:
      return state.merge(action.payload.data.get('data'));
    case REMOVE_USER:
      return initialState;
      case RESET_USER_ERROR:
      return state.setIn(['error'], fromJS({})) // RESET ERROR OBJECT
    default:
      return baseReducer(state, action, USER_ENTITY, 'user');
  }
};
