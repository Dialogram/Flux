import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { persistReducer, persistStore} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import immutableTransform from 'redux-persist-transform-immutable';

import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import createSagaMiddleware from 'redux-saga';
import { sessionReducer } from './Session/Reducer';
import { watcherSaga } from './sagas/index';
import { videoReducer } from './Videos/Reducer';
import { userReducer } from './User/Reducer';
import { documentReducer } from './Documents/Reducer';
import { translationReducer } from './Translations/Reducer';
import { ApiVideoReducer } from './ApiVideo/Reducer';
import { searchReducer } from './Search/Reducer';
import { prefReducer } from './Preferences/Reducer';

import { TranslationVideoReducer } from './TranslationVideo/Reducer';

import { reducer as formReducer } from 'redux-form';

import {
  statusReducer,
} from './statusReducer';

const sagaMiddleware = createSagaMiddleware();

const history = createBrowserHistory();


const persistConfig = {
  version: 1,
  key: 'root',
  storage: storage,
  transforms: [immutableTransform()],
  whitelist: [
    'session',
    'user',
    'documents',
    'TranslationVideo',
  ],
}

// Create this reducer only once. It is not pure!
const reducer = combineReducers({
  session: sessionReducer,
  videos: videoReducer,
  user: userReducer,
  documents: documentReducer,
  translations: translationReducer,
  apivideo: ApiVideoReducer,
  search: searchReducer,
  ui: statusReducer,
  TranslationVideo: TranslationVideoReducer,
  router: connectRouter(history),
  preferences: prefReducer,
  form: formReducer,
})

const persistedReducer = persistReducer(persistConfig, reducer);
const middlewares = [sagaMiddleware, routerMiddleware(history)];

if (process.env.NODE_ENV === 'development') {
  const { logger } = require('redux-logger');

  middlewares.push(logger);
}


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(connectRouter(history)(persistedReducer), composeEnhancers(applyMiddleware(...middlewares)));

sagaMiddleware.run(watcherSaga);

export const loadPersistStore = (cb) => {
  return persistStore(store, null, cb);
};

const dispatch = store.dispatch;
const persistor = persistStore(store);
export {
  dispatch,
  history,
  persistor,
};
export default store;



