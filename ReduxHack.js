import {
  bindActionCreators,
} from 'redux';
import store, {
  dispatch,
} from './redux';
import { LoginFlow } from './Session/Actions';

let ReduxHack = { store, dispatch };

let actions = bindActionCreators({ LoginFlow: LoginFlow,
}, dispatch);

for (let action in actions) {
  ReduxHack[action] = actions[action];
}

export default ReduxHack;
