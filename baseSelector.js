import store from './redux';

export default class baseSelector {
  static getMyId() {
    let myId = store.getState()['user'];
    if (!myId) {
      throw new Error('State initialization error');
    }
    if (!myId.get('user')) {
      return null;
    }
    return myId.get('user').first().get('id');
  }

  static getStatusByName(name) {
    const state = store.getState()['ui'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get(name)) {
      return null;
    }
    return state.get(name).toJS();
  }
}
