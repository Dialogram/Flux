import { takeEvery, takeLatest, put, call, actionChannel, take } from 'redux-saga/effects';

import NavigationFlux from '../navigation';
import DialogramApi from '../../flux/api/Abstraction/Dialogram';

import { success, failure, loading, SuccessApiCall, FailureApiCall, LoadingApiCall, setEntity } from '../baseAction';
import { ApiType, LOG_OUT } from '../baseType';
import { ERROR } from '../error';

import { setSessionEntity, removeSession, createSession, refreshToken } from '../Session/Actions';
import { LOGIN_FLOW } from '../Session/Type';
import { SEARCH_USER_FLOW } from '../Search/Type';

import { removeUser, deleteAccount } from '../User/Actions';
import { DELETE_ACCOUNT_FLOW, USER_ENTITY } from '../User/Type';

import { DOCUMENT_FLOW, DELETE_DOCUMENT_FLOW } from '../Documents/Type';
import { getDocumentById, deleteDocumentById, deleteDocumentFromStore, removeDocuments } from '../Documents/Actions';
import { getDocumentTranslation, createDocumentTranslation } from '../Translations/Actions';
import { CREATE_TRANSLATION_FLOW } from '../../flux/Translations/Type';

import { removeUserList, searchUser } from '../Search/Actions';

import { removeVideosTranslation } from '../TranslationVideo/Actions';


export function* watcherSaga() {
  yield takeLatest(LOG_OUT, LogOutMonitor);
  yield takeLatest(LOGIN_FLOW, loginFlowMonitor);
  yield takeLatest(DOCUMENT_FLOW, documentFlowMonitor);
  yield takeEvery(CREATE_TRANSLATION_FLOW, createTranslationFlow);
  yield takeLatest(DELETE_ACCOUNT_FLOW, deleteAccountFlow);
  yield takeLatest(SEARCH_USER_FLOW, searchUserFlow);
  yield takeLatest(DELETE_DOCUMENT_FLOW, deleteDocumentFlow);

  yield apiSaga2();
}

function* refreshTokenCall() {
  try {
    let act = refreshToken();
    yield put(LoadingApiCall(act.payload.meta.entity));
    yield put(loading(act));
    const response = yield call(new DialogramApi().call, act.payload.meta);
    yield put(setEntity(act, response));
    yield put(SuccessApiCall(act.payload.meta.entity));
    yield put(success(act));
    return response;

  } catch (err) {
    console.log("refresh error", err);
  }
}

function* callApiWithoutRefreshingToken(action) {
  yield put(LoadingApiCall(action.payload.meta.entity));
  yield put(loading(action));
  const response = yield call(new DialogramApi().call, action.payload.meta);
  yield sideEffect(action, response);
  yield put(SuccessApiCall(action.payload.meta.entity));
  yield put(success(action));
  if (action.payload.meta.navigateTo) {
    NavigationFlux.navigate(action.payload.meta.navigateTo, 'replace');
  }

}
function* apiSaga2() {
  const requestChan = yield actionChannel(ApiType.Dialogram)
  while (true) {
    const action = yield take(requestChan)
    try {
      yield put(LoadingApiCall(action.payload.meta.entity));
      yield put(loading(action));
      const response = yield call(new DialogramApi().call, action.payload.meta);
      yield sideEffect(action, response);
      yield put(SuccessApiCall(action.payload.meta.entity));
      yield put(success(action));
      if (action.payload.meta.navigateTo) {
        NavigationFlux.navigate(action.payload.meta.navigateTo, 'replace');
      }
    } catch (error) {
      if (error.data === "jwt expired") {
        try {

          yield call(refreshTokenCall);
          yield call(callApiWithoutRefreshingToken(action));

        } catch (err) {
          yield put(failure(action, error.status, ERROR[error.status]));
          yield put(FailureApiCall(action.payload.meta.entity, error.status, error.data));
        }

      } else {
        yield put(failure(action, error.status, ERROR[error.status]));
        yield put(FailureApiCall(action.payload.meta.entity, error.status, error.data));
      }

      yield put(failure(action, error.status, ERROR[error.status]));
      yield put(FailureApiCall(action.payload.meta.entity, error.status, error.data));
    }

  }
}

// document
export function* deleteDocumentFlow(action) {
  try {
    yield call(apiSaga2, deleteDocumentById(action.payload.meta.id));
    yield put(deleteDocumentFromStore(action.payload.meta.id));
  } catch (e) {
    console.error('deleteDocumentFlow Error: ', e);
  }
}

// user
export function* deleteAccountFlow(action) {
  try {
    yield call(apiSaga2, deleteAccount());
    yield put(removeSession());
    yield put(removeUser());
  } catch (e) {
    console.error('DeleteAccountFlow Error: ', e);
  }
}

// user
export function* searchUserFlow(action) {
  yield put(removeUserList());
  yield call(apiSaga2, searchUser(action.payload.meta));
}

// document
export function* documentFlowMonitor(action) {
  try {
    const response = yield call(apiSaga2, getDocumentById(action.payload.data.id));
    if (!response.get('data').get('documents') || !response.get('data').get('documents').first()) {
      return;
    }
    yield call(apiSaga2, getDocumentTranslation(response.get('data').get('documents').first().toJS()));
  } catch (e) {
    console.error('DocumentFlowMonitor Error: ', e);
  }
}

// session
//TODO Remplacer le dispatch par createSession et delete ça
export function* loginFlowMonitor(action) {
  try {
    const session = yield call(apiSaga2, createSession(action.payload.data));
    if (session === undefined) {
      return;
    }
  } catch (err) {
    console.error('LoginFlowMonitor Error', err)
  }
}


export function* createTranslationFlow(action) {
  try {
    yield call(apiSaga2, createDocumentTranslation({ id: action.payload.meta.idDocument }));
    yield put(getDocumentById(action.payload.meta.idDocument));
  } catch (e) {
    console.error('CreateTranslationFlow Error: ', e);
  }
}

// session
export function* LogOutMonitor(action) {
  yield put(removeSession());
  yield put(removeUser());
  yield put(removeDocuments());
  yield put(removeVideosTranslation());
}

function* sideEffect(action, response) {
  switch (action.payload.meta.entity) {
    case USER_ENTITY:
      yield put(setEntity(action, response));
      yield put(setSessionEntity(response));
      return;
    default:
      yield put(setEntity(action, response));
  }
}
