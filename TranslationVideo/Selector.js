import store from '../../flux/redux';
import baseSelector from '../baseSelector';

export default class VideoTranslationHelper extends baseSelector {
  static getVideoTranslationList() {
    const state = store.getState()['TranslationVideo'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('translation_videos')) {
      return null;
    }
    return state.get('translation_videos').toJS();
  }

  static getVideoTranslationStatus() {
    const state = store.getState()['TranslationVideo'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('status')) {
      return null;
    }
    return state.get('status');
  }

  static getVideoTranslationError() {
    const state = store.getState()['TranslationVideo'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('error')) {
      return null;
    }
    return state.get('error').toJS();
  }

  static getVideoTranslationById({ id }) {
    const state = store.getState()['TranslationVideo'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('translation_videos').get(id)) {
      return null;
    }
    return state.get('translation_videos').get(id).toJS();
  }

  static getPullVideoStatus() {
    return super.getStatusByName('pullVideo');
  }

  static getUploadVideoStatus() {
    return super.getStatusByName('uploadVideo');
  }

  static getDeleteVideoStatus() {
    return super.getStatusByName('deleteVideo');
  }

  static getUpdateVideoStatus() {
    return super.getStatusByName('updateVideo');
  }
}
