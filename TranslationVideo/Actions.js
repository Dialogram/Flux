import { ApiType } from '../baseType';
import { VIDEO_TRANSLATION_ENTITY, REMOVE_VIDEOS_TRANSLATION, ADD_VIDEOS_TRANSLATION_IN_STORE } from './Type';

export const getAllVideoTranslation = () => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: VIDEO_TRANSLATION_ENTITY,
      actionStatusName: 'pullVideo',
      method: 'get',
      url: '/api/user/translationVideos',
    },
  },
});

export const getVideoTranslationById = ({ id }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: VIDEO_TRANSLATION_ENTITY,
      actionStatusName: 'pullVideo',
      method: 'get',
      url: `/api/translationVideo/${id}`,
    },
  },
});

export const deleteVideoTranslationById = ({ id }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: VIDEO_TRANSLATION_ENTITY,
      actionStatusName: 'deleteVideo',
      method: 'delete',
      url: `/api/video/${id}`,
    },
  },
});

export const uploadVideoTrad = ({video}) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: VIDEO_TRANSLATION_ENTITY,
      actionStatusName: 'uploadVideo',
      method: 'post',
      url: '/api/translationVideo',
      body: video,
    },
    data: {},
  },
})
//to Delete After merge
export const uploadVideoTranslationDialogram = (video) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: VIDEO_TRANSLATION_ENTITY,
      actionStatusName: 'uploadVideo',
      method: 'post',
      url: '/api/translationVideo',
      body: video,
    },
    data: {},
  },
});

export const addVideoTranslationInStore = (video) => ({
  type: ADD_VIDEOS_TRANSLATION_IN_STORE,
  payload: {
    meta: {
      entity: VIDEO_TRANSLATION_ENTITY,
    },
    data: video,
  },
});

export const removeVideosTranslation = () => ({
  type: REMOVE_VIDEOS_TRANSLATION,
});
