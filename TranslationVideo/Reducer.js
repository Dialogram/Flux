import { fromJS } from 'immutable';
import { Status } from '../baseType';
import { VIDEO_TRANSLATION_ENTITY, REMOVE_VIDEOS_TRANSLATION, ADD_VIDEOS_TRANSLATION_IN_STORE } from './Type';
import { CLEAR_VIDEO_IN_STORE } from '../Videos/Type';
import { baseReducer } from '../baseReducer';

const initialState = fromJS({
  status: Status.Default,
  error: {},
  videos: {},
  translation_videos: {},
});

export const TranslationVideoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_VIDEOS_TRANSLATION_IN_STORE:
      return state.setIn(['videos'], action.payload.data);
    case REMOVE_VIDEOS_TRANSLATION:
      return initialState;
    case CLEAR_VIDEO_IN_STORE:
      return state.deleteIn(['videos', action.payload.data.id]);
    default:
      return baseReducer(state, action, VIDEO_TRANSLATION_ENTITY, 'videos');
  }
};
