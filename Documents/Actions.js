import { ApiType } from '../baseType';
import { DOCUMENTS_ENTITY, REMOVE_DOCUMENTS, DOCUMENT_FLOW, DELETE_DOCUMENT_FLOW, REMOVE_DOCUMENT_BY_ID, EXPLORE_DOCUMENT_ENTITY } from './Type';

export const documentFlowById = (document) => ({
  type: DOCUMENT_FLOW,
  payload: {
    meta: {},
    data: document,
  },
});

export const exploreDocument = ({ limit, page, offset, category, field, sort }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: EXPLORE_DOCUMENT_ENTITY,
      actionStatusName: 'pullDocument',
      method: 'get',
      url: `/api/search/?limit=${limit}&page=${page}&category=${category || 'undefined'}&sort=${field || 'undefined'}${',' + sort || ''}`,
    },
    data: {},
  },
});

export const searchDocument = ({ toFind }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'pullDocument',
      method: 'get',
      url: `/api/document/search/${toFind}`,
      type: 'documents',
    },
    data: {},
  },
});

export const getAllDocument = ({ limit }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'pullDocument',
      method: 'get',
      url: `/api/user/documents?limit=${limit}`,
      type: 'documents',
    },
  },
});

export const getDocumentById = (idDocument) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'pullDocument',
      method: 'get',
      url: `/api/document/${idDocument}`,
    },
  },
});

export const documentPdfUpdate = ({ id }, { name, description, publicDoc, category }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'updateDocument',
      method: 'put',
      url: `/api/document/update/${id}`,
      body: { 'name': name, 'description': description, 'public': publicDoc, category },
    },
  },
});

export const postDocument = (formData) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'uploadDocument',
      method: 'POST',
      url: '/api/document',
      body: formData,
    },
  },
});

export const getDocumentByCat = (cat) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'pullDocument',
      method: 'GET',
      url: `/api/search/?category=${cat}`,
    },
  },
});

export const deleteDocumentFromStore = (id) => ({
  type: REMOVE_DOCUMENT_BY_ID,
  payload: {
    meta: {
      id,
    },
    data: {},
  },
});

export const deleteDocumentFlow = (id) => ({
  type: DELETE_DOCUMENT_FLOW,
  payload: {
    meta: {
      id,
    },
    data: {},
  },
});

export const deleteDocumentById = (id) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'deleteDocument',
      method: 'DELETE',
      url: `/api/document/${id}`,
    },
  },
});

export const removeDocuments = () => ({
  type: REMOVE_DOCUMENTS,
});

export const likeDocument = ({ id }/* id Document */) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'documentFeatures',
      method: 'PUT',
      url: `/api/document/${id}/like`,
    },
  },
});

export const unLikeDocument = ({ id }/* id Document */) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'documentFeatures',
      method: 'PUT',
      url: `/api/document/${id}/unlike`,
    },
  },
});

export const favoriteDocument = ({ id }/* id Document */) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'documentFeatures',
      method: 'PUT',
      url: `/api/document/${id}/favorite`,
    },
  },
});

export const unFavoriteDocument = ({ id }/* id Document */) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'documentFeatures',
      method: 'PUT',
      url: `/api/document/${id}/unfavorite`,
    },
  },
});

export const commentDocument = ({ id }, comment/* id Document */) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'documentFeatures',
      method: 'PUT',
      url: `/api/document/${id}/comment`,
      body: comment,
    },
  },
});

export const promoteUser = ({ idUser, role, idDocument }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'documentPermission',
      method: 'PUT',
      url: `/api/document/${idDocument}/addAccess/${idUser}/${role}`
    },
  },
})


export const demoteUser = ({ idUser, role, idDocument }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'documentPermission',
      method: 'DELETE',
      url: `/api/document/${idDocument}/addAccess/${idUser}/${role}`
    },
  },
})

/// to delete
export const giveEditPermission = ({ id }, user /* id Document */) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'documentPermission',
      method: 'PUT',
      url: `/api/document/${id}/waccess`,
      body: {
        user,
      },
    },
  },
});

export const giveDeletionPermission = ({ id }, user /* id Document */) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'documentPermission',
      method: 'PUT',
      url: `/api/document/${id}/daccess`,
      body: {
        user,
      },
    },
  },
});

export const removeEditPermission = ({ id }, user /* id Document */) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'documentPermission',
      method: 'delete',
      url: `/api/document/${id}/waccess`,
      body: {
        user,
      },
    },
  },
});

export const removeDeletionPermission = ({ id }, user /* id Document */) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: DOCUMENTS_ENTITY,
      actionStatusName: 'documentPermission',
      method: 'delete',
      url: `/api/document/${id}/daccess`,
      body: {
        user,
      },
    },
  },
});
