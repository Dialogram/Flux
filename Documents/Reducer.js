import { fromJS } from 'immutable';
import { Status, SET_ENTITY } from '../baseType';
import { DOCUMENTS_ENTITY, REMOVE_DOCUMENTS, REMOVE_DOCUMENT_BY_ID, EXPLORE_DOCUMENT_ENTITY } from './Type';
import { baseReducer } from '../baseReducer';

const initialState = fromJS({
  status: Status.Default,
  error: {},
  documents: {},
});

export const documentReducer = (state = initialState, action) => {
  switch (action.type) {
    case REMOVE_DOCUMENT_BY_ID:
      return state.deleteIn(['documents', action.payload.meta.id]);
    case REMOVE_DOCUMENTS:
      return initialState;
    case `${EXPLORE_DOCUMENT_ENTITY} ${SET_ENTITY}`:
      const merged = action.payload.data.get('data').merge(action.payload.data.get('includes'));
      state.setIn(['explore'], fromJS({}));
      if (!merged.first() || !merged.first().first() || !merged.first().first().get('type')) { return state; }
      const data = merged.toJS();
      for (let o in data) {
        const array = Object.keys(data[o]);
        let type = data[o][array[0]].type === 'pagination' ? 'pagination' : 'explore';
        state = state.merge(fromJS({ [type]: data[o] }));
      }
      return state;
    default:
      return baseReducer(state, action, DOCUMENTS_ENTITY, 'documents');
  }
};
