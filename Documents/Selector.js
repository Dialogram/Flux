import store from '../../flux/redux';
import Immutable from 'immutable';
import baseSelector from '../baseSelector';

export default class DocumentHelper extends baseSelector {
  static getDocumentById(id) {
    //  const myDocument = store.getState()['documents'].get('myDocuments');
    const document = store.getState()['documents'].get('documents');
    /* if (!myDocument || !Immutable.Map.isMap(myDocument)) {
            return null;
        } */
    if (!document || !Immutable.Map.isMap(document)) {
      return null;
    }
    /* if (myDocument.get(id)) { return myDocument.get(id).toJS(); }
        else */if (document.get(id)) {
      return document.get(id).toJS();
    }
    return null;
  }
  static getExploreDocument() {
    const document = store.getState()['documents'].get('explore');
    if (!document || !Immutable.Map.isMap(document)) {
      return null;
    }
    return document.toJS();
  }
  static getExplorerDocumentByCat(cat) {
    const document = store.getState()['documents'].get('explore');
    if (!document || !Immutable.Map.isMap(document)) {
      return null;
    }
    let doc = document.toJS();
    let array = Object.values(doc);
    return array.filter(doc => {
      if (doc.category === cat) {
        return true;
      }
      return false;
    });
  }

  static getAllDocument() { // TODO CHANGER EN GETMYDOCUMENT
    const document = store.getState()['documents'].get('documents');
    if (!document || !Immutable.Map.isMap(document)) {
      return null;
    }
    return document.toJS();
  }

  static getDocumentStatus() {
    const state = store.getState()['documents'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('status')) {
      return null;
    }
    return state.get('status');
  }

  static getDocumentError() {
    const state = store.getState()['documents'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('error')) {
      return null;
    }
    return state.get('error').toJS();
  }

  static getMyDocuments() {
    const myId = DocumentHelper.getMyId();
    const document = store.getState()['documents'].get('documents');
    if (!document || !Immutable.Map.isMap(document)) {
      return null;
    }
    let doc = document.toJS();
    let array = Object.values(doc);
    return array.filter(doc => {
      if (doc.idOwner === myId) {
        return true;
      }
      return false;
    });
  }

  static getDocumentByCat(cat) {
    const document = store.getState()['documents'].get('documents');
    if (!document || !Immutable.Map.isMap(document)) {
      return null;
    }
    let doc = document.toJS();
    let array = Object.values(doc);
    return array.filter(doc => {
      if (doc.category === cat) {
        return true;
      }
      return false;
    });
  }

  static getDocumentSortByLike() {
    const document = store.getState()['documents'].get('documents');
    if (!document || !Immutable.Map.isMap(document)) {
      return null;
    }
    let doc = document.toJS();
    let array = Object.values(doc);
    return array.sort((a, b) => {
      return b.features.likes.length - a.features.likes.length;
    });
  }

  static getFavoriteDocument() {
    const myId = DocumentHelper.getMyId();
    const document = store.getState()['documents'].get('documents');
    if (!document || !Immutable.Map.isMap(document)) {
      return null;
    }
    let doc = document.toJS();
    let array = Object.values(doc);
    return array.filter(doc => {
      if (doc.features.favorites.findIndex(el => el === myId) !== -1) {
        return true;
      }
      return false;
    });
  }

  static getLikedDocument() {
    const myId = DocumentHelper.getMyId();
    const document = store.getState()['documents'].get('documents');
    if (!document || !Immutable.Map.isMap(document)) {
      return null;
    }
    let doc = document.toJS();
    let array = Object.values(doc);
    return array.filter(doc => {
      if (doc.features.likes.findIndex(el => el === myId) !== -1) {
        return true;
      }
      return false;
    });
  }

  static getPullDocumentStatus() {
    return super.getStatusByName('pullDocument');
  }

  static getUploadDocumentStatus() {
    return super.getStatusByName('uploadDocument');
  }

  static getDeleteDocumentStatus() {
    return super.getStatusByName('deleteDocument');
  }

  static getUpdateDocumentStatus() {
    return super.getStatusByName('updateDocument');
  }

  static getDocumentFeatureStatus() {
    return super.getStatusByName('documentFeatures');
  }
}
