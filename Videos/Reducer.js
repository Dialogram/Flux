import { fromJS } from 'immutable';
import { Status } from '../baseType';
import { VIDEO_ENTITY, REMOVE_VIDEOS, ADD_VIDEOS_IN_STORE, CLEAR_VIDEO_IN_STORE } from './Type';
import { baseReducer } from '../baseReducer';

const initialState = fromJS({
  status: Status.Default,
  error: {},
  videos: {},
});

export const videoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_VIDEOS_IN_STORE:
      return state.setIn(['videos'], action.payload.data);
    case REMOVE_VIDEOS:
      return initialState;
    case CLEAR_VIDEO_IN_STORE:
      return state.deleteIn(['videos', action.payload.data]);
    default:
      return baseReducer(state, action, VIDEO_ENTITY, 'videos');
  }
};
