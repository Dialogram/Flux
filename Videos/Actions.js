import { ApiType } from '../baseType';
import { VIDEO_ENTITY, REMOVE_VIDEOS, ADD_VIDEOS_IN_STORE, REMOVE_VIDEOS_FLOW, CLEAR_VIDEO_IN_STORE } from './Type';

export const getAllVideo = () => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: VIDEO_ENTITY,
      actionStatusName: 'pullVideo',
      method: 'get',
      url: '/api/user/video',
    },
  },
});

export const getVideoById = ({ id }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: VIDEO_ENTITY,
      actionStatusName: 'pullVideo',
      method: 'get',
      url: `/api/video/${id}`,
    },
  },
});

export const deleteVideoById = ({ id }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: VIDEO_ENTITY,
      actionStatusName: 'deleteVideo',
      method: 'delete',
      url: `/api/video/${id}`,
    },
  },
});

export const uploadVideoDialogram = (video) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: VIDEO_ENTITY,
      actionStatusName: 'uploadVideo',
      method: 'post',
      url: '/api/video',
      body: video,
    },
    data: {},
  },
});

export const addVideoInStore = (video) => ({
  type: ADD_VIDEOS_IN_STORE,
  payload: {
    meta: {
      entity: VIDEO_ENTITY,
    },
    data: video,
  },
});
/*
export const removeVideos = () => ({
    type: REMOVE_VIDEOS,
}); */

export const removeVideoFlow = (video) => ({
  type: REMOVE_VIDEOS_FLOW,
  payload: {
    meta: {
      entity: VIDEO_ENTITY,
    },
    data: video,
  },
});

export const removeVideoFromApiVideo = (id) => ({
  type: REMOVE_VIDEOS,
  payload: {
    meta: {
      entity: VIDEO_ENTITY,
      method: 'DELETE',
      url: `/videos/${id}`,
    },

  },

});

export const cleanVideoInStore = (id) => ({
  type: CLEAR_VIDEO_IN_STORE,
  payload: {
    meta: {},
    data: id,
  },
});
