import store from '../../flux/redux';
import baseSelector from '../baseSelector';

export default class VideoHelper extends baseSelector {
  static getVideoList() {
    const state = store.getState()['videos'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('videos')) {
      return null;
    }
    return state.get('videos').toJS();
  }

  static getVideoStatus() {
    const state = store.getState()['videos'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('status')) {
      return null;
    }
    return state.get('status');
  }

  static getVideoError() {
    const state = store.getState()['videos'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('error')) {
      return null;
    }
    return state.get('error').toJS();
  }

  static getVideoById({ id }) {
    const state = store.getState()['videos'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('videos').get(id)) {
      return null;
    }
    return state.get('videos').get(id).toJS();
  }

  static getPullVideoStatus() {
    return super.getStatusByName('pullVideo');
  }

  static getUploadVideoStatus() {
    return super.getStatusByName('uploadVideo');
  }

  static getDeleteVideoStatus() {
    return super.getStatusByName('deleteVideo');
  }

  static getUpdateVideoStatus() {
    return super.getStatusByName('updateVideo');
  }
}
