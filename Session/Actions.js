import { ApiType } from '../baseType';
import { SESSION_ENTITY, REMOVE_SESSION, SET_SESSION, LOGIN_FLOW } from './Type';

import SessionHelper from './Selector';

export const LoginFlow = ({ email, password, from }) => ({
  type: LOGIN_FLOW,
  payload: {
    meta: {},
    data: { email, password, from },
  },
});
export const refreshToken = () => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: SESSION_ENTITY,
      actionStatusName: 'login',
      method: 'get',
      url: `/api/account/refresh-token/${SessionHelper.getRefreshToken()}`,
      body: {}
    },
  },
});
export const createSession = ({ email, password, from }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: SESSION_ENTITY,
      actionStatusName: 'login',
      method: 'post',
      url: '/api/session',
      body: {
        'email': email,
        'password': password,
        from,
      },
      navigateTo: from,
    },
  },
});

export const forgotPassword = (email) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: SESSION_ENTITY,
      actionStatusName: 'updatePassword',
      method: 'post',
      url: '/api/password/reset',
      body: {
        email,
      },
    },
    data: {},
  },
});

export const forgotPasswordConfirm = (token, mdp) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: SESSION_ENTITY,
      actionStatusName: 'updatePassword',
      method: 'post',
      url: `/api/password/reset/${token}`,
      body: {
        'password': mdp,
        'confirm': mdp,
      },
    },
    data: {},
  },
});

export const setSessionEntity = (response) => ({
  type: SET_SESSION,
  payload: {
    meta: {
      entity: SESSION_ENTITY,
    },
    data: response,
  },
});

export const removeSession = () => ({
  type: REMOVE_SESSION,
  payload: {
    meta: {},
    data: {},
  },
});
