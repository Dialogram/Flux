import store from '../redux';
import Immutable from 'immutable';
import { removeSession } from './Actions';
import NavigationFlux from '../navigation';
import baseSelector from '../baseSelector';


export default class SessionHelper extends baseSelector {
  // use only for sideEffect don't use inside View
  static deleteSessionData() {
    store.dispatch(removeSession());
    NavigationFlux.navigate('auth', 'replace');
  }

  static hasSessionActive() {
    return !!store.getState()['session'].get('session');
  }

  static getRefreshToken() {
    const session = store.getState()['session'].get('session');
    if (!session || !Immutable.Map.isMap(session)) {
      return null;
    }
    return session.first().get('refreshToken');
  }

  static getHeaderAuthorization() {
    const session = store.getState()['session'].get('session');
    if (!session || !Immutable.Map.isMap(session)) {
      return null;
    }
    return `Bearer ${session.first().get('token')}`;
  }

  static getSessionStatus() {
    const state = store.getState()['session'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('status')) {
      return null;
    }
    return state.get('status');
  };

  static getSessionToken() {
    const state = store.getState()['session'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('session')) {
      return null;
    }
    if (!state.get('session').first()) { return null; }
    return state.get('session').first().get('token');
  }

  static getSessionId() {
    const state = store.getState()['session'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('session')) {
      return null;
    }
    if (!state.get('session').first()) { return null; }
    return state.get('session').first().get('id');
  }

  static getSessionUserId() {
    const state = store.getState()['session'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('session')) {
      return null;
    }
    if (!state.get('session').first()) { return null; }
    return state.get('session').first().get('user');
  }

  static getSessionError() {
    const state = store.getState()['session'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('error')) {
      return {};
    }
    return state.get('error').toJS();
  }

  static getLoginStatus() {
    return super.getStatusByName('login');
  }

  static getLogoutStatus() {
    return super.getStatusByName('logout');
  }
}
