export const SESSION_ENTITY = 'SESSION';
export const REMOVE_SESSION = 'REMOVE_SESSION';
export const SET_SESSION = 'SET_SESSION';
export const LOGIN_FLOW = 'LOGIN_FLOW';
