import { fromJS } from 'immutable';
import { Status, SET_ENTITY } from '../baseType';
import { SESSION_ENTITY, REMOVE_SESSION, SET_SESSION } from './Type';
import { baseReducer } from '../baseReducer';

export const initialState = fromJS({
  status: Status.Default,
  error: {},
});

export const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SESSION:
      return state.merge(action.payload.data.get('includes'));
    case `${SESSION_ENTITY} ${SET_ENTITY}`:
      return state.merge(action.payload.data.get('data'));
    case REMOVE_SESSION:
      return initialState;
    default:
      return baseReducer(state, action, SESSION_ENTITY, 'session');
  }
};
