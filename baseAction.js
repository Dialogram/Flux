import { Status, SET_ENTITY, LOG_OUT } from './baseType';

export const SuccessApiCall = (entity) => ({
  type: `${entity} ${Status.Success}`,
});

export const FailureApiCall = (entity, code, message) => ({
  type: `${entity} ${Status.Failure}`,
  payload: {
    data: {
      code,
      message,
    },
  },
});

export const LoadingApiCall = (entity) => ({
  type: `${entity} ${Status.Loading}`,
});

export const setEntity = (action, response) => ({
  ...action,
  type: `${action.payload.meta.entity} ${SET_ENTITY}`,
  payload: {
    meta: action.payload.meta,
    data: response,
  },
});

export const Logout = () => ({
  type: LOG_OUT,
});

export const success = (action) => ({
  ...action,
  type: 'Success',
});

export const failure = (action, code, message) => ({
  ...action,
  type: 'Failure',
  payload: {
    ...action.payload,
    data: {
      ...action.payload.data,
      code,
      message,
    },
  },
});

export const loading = (action) => ({
  ...action,
  type: 'Loading',
});
