import store from '../../flux/redux';
import baseSelector from '../baseSelector';

export default class PreferencesHelper extends baseSelector {
  static getPrefs() {
    const state = store.getState()['preferences'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('explorer')) {
      return null;
    }
    return state.get('explorer').toJS();
  }
}
