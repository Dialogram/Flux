import { fromJS } from 'immutable';
import { Status } from '../baseType';
import { SAVE_PREF } from './Type';

export const initialState = fromJS({
  status: Status.Default,
  error: {},
});

export const prefReducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_PREF:
      const body = action.payload.meta.body;
      if (body === null) { return state; }

      return state.setIn(['explorer'], fromJS(body));
    default:
      return state;
  }
};
