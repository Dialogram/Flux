import { SAVE_PREF } from './Type';

export const savePrefs = (prefs) => ({
  type: SAVE_PREF,
  payload: {
    meta: {
      entity: SAVE_PREF,
      body: prefs,
    },
  },
});
