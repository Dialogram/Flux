import { fromJS } from 'immutable';
import { Status, SET_ENTITY } from './baseType';

export function baseReducer(state, action, entity) {
  switch (action.type) {
    case `${entity} ${SET_ENTITY}`:
      const merged = action.payload.data.get('data').merge(action.payload.data.get('includes'));
      if (!merged.first() || !merged.first().first() || !merged.first().first().get('type')) { return state; }
      const data = merged.toJS();
      for (let o in data) {
        const array = Object.keys(data[o]);
        for (let ent in array) {
          const type = data[o][array[ent]].type;
          state = state.setIn([type, array[ent]], fromJS(data[o][array[ent]]));
        }
      }
      return state;

    case `${entity} ${Status.Failure}`:
      return state.setIn(['status'], Status.Failure)
        .setIn(['error'], fromJS({ code: action.payload.data.code, message: action.payload.data.message }));
    case `${entity} ${Status.Loading}`:
      return state.setIn(['status'], Status.Loading);
    case `${entity} ${Status.Success}`:
      return state.setIn(['status'], Status.Success).setIn(['error'], fromJS({}));
    default:
      return state;
  }
}
