export const ERROR = {
  '500': 'Nou avons un problème avec notre serveur, essayez plus tard',
  '400': 'Les informations données sont incorrect', // Bad Request -- Your request is invalid.
  '401': "Accès interdit, les informations de votre requête ne vous permettent pas d'accéder à cette ressource.", // Unauthorized -- The informations/structure of your request does not permit you to use this route.
  '403': "Accès interdit, vous n'avez pas la permission d'accéder à cette ressource.", // Forbidden -- You don't have enough permissions to perform this request.
  '404': 'La ressource demandé est introuvable', // Not Found -- The specified resource could not be found.
  '405': "Vous essayez d'envoyer un requête avec une méthode invalide", // Method Not Allowed -- You tried to perform a request with an invalid method.
  '406': "Le format de la requête n'est pas en JSON", // Not Acceptable -- You requested a format that isn't json.
  '410': 'La ressource a été effacé du serveur', // Gone -- The resource requested has been removed from our servers.
  '418': 'Je suis un pot de thé', // I'm a teapot.
  '429': 'Trop de requêtes', // Too Many Requests -- You're performing too many requests! Slow down!
  '503': 'Service indisponible', // Service Unavailable -- We're temporarily offline for maintenance. Please try again later.
};
