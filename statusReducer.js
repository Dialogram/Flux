import { fromJS } from 'immutable';

export const initialState = fromJS({
  login: { status: 'Default', error: {} },
  logout: { status: 'Default', error: {} },
  register: { status: 'Default', error: {} },
  deleteAccount: { status: 'Default', error: {} },
  updatePassword: { status: 'Default', error: {} },
  updateAccount: { status: 'Default', error: {} },

  pullDocument: { status: 'Default', error: {} },
  uploadDocument: { status: 'Default', error: {} },
  deleteDocument: { status: 'Default', error: {} },
  updateDocument: { status: 'Default', error: {} },
  documentFeatures: { status: 'Default', error: {} },

  pullTraduction: { status: 'Default', error: {} },
  uploadTraduction: { status: 'Default', error: {} },
  deleteTraduction: { status: 'Default', error: {} },
  updateTraduction: { status: 'Default', error: {} },

  pullVideo: { status: 'Default', error: {} },
  uploadVideo: { status: 'Default', error: {} },
  deleteVideo: { status: 'Default', error: {} },
  updateVideo: { status: 'Default', error: {} },

  postVideo: { status: 'Default', error: {} },

  searchUser: { status: 'Default', error: {} },

  documentPermission: { status: 'Default', error: {} },
});

export const statusReducer = (state = initialState, action) => {
  if (!action || !action.payload || !action.payload.meta || !action.payload.meta.actionStatusName) { return state; }
  const { actionStatusName } = action.payload.meta;
  switch (action.type) {
    case `Loading`:
      return state.setIn([actionStatusName], fromJS({ status: 'Loading', error: {} }));
    case `Success`:
      return state.setIn([actionStatusName], fromJS({ status: 'Success', error: {} }));
    case `Failure`:
      return state.setIn([actionStatusName], fromJS({ status: 'Failure', error: { code: action.payload.data.code, message: action.payload.data.message } }));
    default:
      return state;
  }
};
