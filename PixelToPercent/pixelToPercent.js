
export const toPercent = (len, value) => {
    return value * 100 / len;
}

export const toPixel = (len, value) => {
    return value * len / 100;
}
