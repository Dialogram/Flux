export const TRADUCTIONS_ENTITY = 'TRADUCTIONS';
export const REMOVE_TRADUCTIONS = 'REMOVE_TRADUCTIONS';

export const SCOPE = {
  addTranslationInStore: '[ADD_TRANSLATION_IN_STORE]',
  removeTranslationFromStore: '[REMOVE_TRANSLATION _FROM_STORE',
  bindVideo: '[BIND_VIDEO]',
};
export const CREATE_TRANSLATION_FLOW = '[CREATE_TRANSLATION_FLOW]';
