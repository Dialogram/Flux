import { ApiType } from '../baseType';
import { TRADUCTIONS_ENTITY, REMOVE_TRADUCTIONS, SCOPE, CREATE_TRANSLATION_FLOW } from './Type';

export const getDocumentTranslation = ({ id, idTranslation }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: TRADUCTIONS_ENTITY,
      actionStatusName: 'pullTraduction',
      method: 'GET',
      url: `/api/document/${id}/translation/${idTranslation}`,
    },
    data: {
    },
  },
});

export const createDocumentTranslationFlow = ({ id }) => ({
  type: CREATE_TRANSLATION_FLOW,
  payload: {
    meta: {
      idDocument: id,
    },
    data: {},
  },
});

export const createDocumentTranslation = ({ id }) => ({ // id is document id
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: TRADUCTIONS_ENTITY,
      actionStatusName: 'uploadTraduction',
      method: 'POST',
      url: `/api/document/${id}/translation`,
      body:
      {
        'translation': [
        ],
      }
      ,
    },
    data: {},
  },
});

export const bindVideoToTrad = (linkTrad, index, page, idTranslation) => ({
  type: SCOPE.bindVideo,
  payload: {
    meta: {
      entity: TRADUCTIONS_ENTITY,
    },
    data: {
      linkTradz: linkTrad,
      index,
      page,
      idTranslation,
    },
  },
});

export const addTranslationInStore = (translation, page, data, index) => ({
  type: SCOPE.addTranslationInStore,
  payload: {
    meta: {
      entity: TRADUCTIONS_ENTITY,
      idTranslation: translation.id,
      page,
      index,
    },
    data: data,
  },
});

export const removeTranslationFromStore = (translation, page, index) => ({
  type: SCOPE.removeTranslationFromStore,
  payload: {
    meta: {
      entity: TRADUCTIONS_ENTITY,
      idTranslation: translation.id,
      page,
      index,
    },
    data: {},
  },
});

export const removeTranslations = () => ({
  type: REMOVE_TRADUCTIONS,
});

export const updateTranslation = ({ id, idDocument, translation }) => ({
  type: ApiType.Dialogram,
  payload: {
    meta: {
      entity: TRADUCTIONS_ENTITY,
      actionStatusName: 'updateTraduction',
      method: 'PUT',
      url: `/api/document/${idDocument}/translation/${id}`,
      body: { translation },
    },
    data: {},
  },
});
