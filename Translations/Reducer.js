import { fromJS } from 'immutable';
import { Status } from '../baseType';
import { TRADUCTIONS_ENTITY, REMOVE_TRADUCTIONS, SCOPE } from './Type';
import { baseReducer } from '../baseReducer';

const initialState = fromJS({
  status: Status.Default,
  error: {},
  translations: {},
});

export const translationReducer = (state = initialState, action) => {
  switch (action.type) {
    case REMOVE_TRADUCTIONS:
      return initialState;
    case SCOPE.removeTranslationFromStore:

      return state.deleteIn(['translations', action.payload.meta.idTranslation, 'translation', action.payload.meta.page, action.payload.meta.index]);
    case SCOPE.addTranslationInStore:
      let translation = state.get('translations');
      translation = translation.get(action.payload.meta.idTranslation);
      if (!translation) {
        return state;
      }
      translation = translation.get('translation');
      if (!translation) {
        return state;
      }
      if (!Array.isArray(translation.toJS())) {
        state = state.setIn(['translations', action.payload.meta.idTranslation, 'translation'], fromJS([[]]));
      }
      if (action.payload.meta.index !== undefined) {
        return state.setIn(['translations', action.payload.meta.idTranslation, 'translation', action.payload.meta.page, action.payload.meta.index], fromJS(action.payload.data));
      }
      return state.updateIn(['translations', action.payload.meta.idTranslation, 'translation', action.payload.meta.page], arr => arr.push(fromJS(action.payload.data)));

    default:
      return baseReducer(state, action, TRADUCTIONS_ENTITY, 'translations');
  }
};
