import store from '../../flux/redux';
import baseSelector from '../baseSelector';

export default class TraductionHelper extends baseSelector {
  static getTranslationForDoc(id) {
    const doc = store.getState()['documents'].get('documents');
    if (!doc || !id) {
      return null;
    }

    if (!doc.get(id)) {
      return null;
    }
    const transId = doc.get(id);
    const translation = store.getState()['translations'].get('translations');
    if (!translation || !transId) {
      return null;
    }
    if (!translation.get(transId.get('idTranslation'))) {
      return null;
    }
    return translation.get(transId.get('idTranslation')).toJS();
  }

  static getTranslationStatus() {
    const state = store.getState()['translations'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('status')) {
      return null;
    }
    return state.get('status');
  }

  static getTranslationError() {
    const state = store.getState()['translations'];
    if (!state) {
      throw new Error('State initialization error');
    }
    if (!state.get('error')) {
      return null;
    }
    return state.get('error').toJS();
  }

  static getPullTraductionStatus() {
    return super.getStatusByName('pullTraduction');
  }

  static getUploadTraductionStatus() {
    return super.getStatusByName('uploadTraduction');
  }

  static getDeleteTraductionStatus() {
    return super.getStatusByName('deleteTraduction');
  }

  static getUpdateTraductionStatus() {
    return super.getStatusByName('updateTraduction');
  }
}
